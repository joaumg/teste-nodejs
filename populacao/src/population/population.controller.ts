import { Controller, Get, NotFoundException, Param } from '@nestjs/common';
import { GrpcMethod } from '@nestjs/microservices';

declare type StatePopulation = {
  uf: string;
  populacao: number;
};

@Controller('populacao')
export class PopulationController {
  // https://pt.wikipedia.org/wiki/Unidades_federativas_do_Brasil
  private readonly allStates = [
    {
      uf: 'AC',
      populacao: 795145,
    },
    {
      uf: 'AL',
      populacao: 3327551,
    },
    {
      uf: 'AP',
      populacao: 756500,
    },
    {
      uf: 'AM',
      populacao: 3893763,
    },
    {
      uf: 'BA',
      populacao: 15150143,
    },
    {
      uf: 'CE',
      populacao: 8867448,
    },
    {
      uf: 'DF',
      populacao: 2867869,
    },
    {
      uf: 'ES',
      populacao: 3894899,
    },
    {
      uf: 'GO',
      populacao: 6551322,
    },
    {
      uf: 'MA',
      populacao: 6861924,
    },
    {
      uf: 'MT',
      populacao: 3236578,
    },
    {
      uf: 'MS',
      populacao: 2630098,
    },
    {
      uf: 'MG',
      populacao: 20777672,
    },
    {
      uf: 'PA',
      populacao: 8101180,
    },
    {
      uf: 'PB',
      populacao: 3950359,
    },
    {
      uf: 'PR',
      populacao: 11112062,
    },
    {
      uf: 'PE',
      populacao: 9297861,
    },
    {
      uf: 'PI',
      populacao: 3198185,
    },
    {
      uf: 'RJ',
      populacao: 16497395,
    },
    {
      uf: 'RN',
      populacao: 3419550,
    },
    {
      uf: 'RS',
      populacao: 11228091,
    },
    {
      uf: 'RO',
      populacao: 1755015,
    },
    {
      uf: 'RR',
      populacao: 500826,
    },
    {
      uf: 'SC',
      populacao: 6734568,
    },
    {
      uf: 'SP',
      populacao: 44169350,
    },
    {
      uf: 'SE',
      populacao: 2227294,
    },
    {
      uf: 'TO',
      populacao: 1502759,
    },
  ];

  // {"uf":"SP", "populacao" : 45538936}
  @Get(':id')
  fetch(@Param('id') uf: string): StatePopulation {
    const state = this.allStates.filter((state) => {
      return state.uf === uf.toUpperCase();
    });
    if (state.length !== 1) {
      throw new NotFoundException();
    }
    return state[0];
  }

  @GrpcMethod('PopulationService', 'FetchAll')
  fetchAll(): { data: StatePopulation[] } {
    return { data: this.allStates };
  }
}
