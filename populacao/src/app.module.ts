import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PopulationController } from './population/population.controller';

@Module({
  imports: [],
  controllers: [AppController, PopulationController],
  providers: [AppService],
})
export class AppModule {}
