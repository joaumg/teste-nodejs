import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { join } from 'path';

import { AppModule } from './app.module';

async function bootstrap() {
  const grpcServer = await NestFactory.createMicroservice<MicroserviceOptions>(
    AppModule,
    {
      transport: Transport.GRPC,
      options: {
        package: 'population',
        protoPath: join(__dirname, 'population/population.proto'),
      },
    },
  );
  await grpcServer.listenAsync();
  // Hybrid Server (gRPC + HTTP)
  const httpServer = await NestFactory.create(AppModule);
  await httpServer.listen(process.env.PORT || 3000);
}

bootstrap();
