import * as ProtoLoader from '@grpc/proto-loader';
import * as GRPC from 'grpc';
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { Transport } from '@nestjs/microservices';
import { join } from 'path';

import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let server;
  let client: any;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    const protoPath = join(__dirname, '../src/population/population.proto');
    app = moduleFixture.createNestApplication();
    server = app.getHttpAdapter().getInstance();
    app.connectMicroservice({
      transport: Transport.GRPC,
      options: {
        package: ['population'],
        protoPath: [protoPath],
      },
    });
    // Start gRPC microservice
    await app.startAllMicroservicesAsync();
    await app.init();
    // Load proto-buffers for test gRPC dispatch
    const proto = ProtoLoader.loadSync(protoPath) as any;
    // Create Raw gRPC client object
    const protoGRPC = GRPC.loadPackageDefinition(proto) as any;
    // Create client connected to started services at standard 5000 port
    client = new protoGRPC.population.PopulationService(
      'localhost:5000',
      GRPC.credentials.createInsecure(),
    );
  });

  afterAll(async () => {
    await app.close();
  });

  it('GET /populacao/:id', () => {
    return request(server)
      .get('/populacao/sp')
      .expect(200)
      .expect('{"uf":"SP","populacao":44169350}');
  });

  it('gRPC fetchAll', async () => {
    const response: any = await new Promise((resolve, reject) => {
      client.FetchAll({}, (err: any, res: any) => {
        if (err) {
          reject(err);
        } else {
          resolve(res);
        }
      });
    });
    expect(response).toHaveProperty('data');
    expect(response.data).toBeArrayOfSize(26 + 1);
  });
});
