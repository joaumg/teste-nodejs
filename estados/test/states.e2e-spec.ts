import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('GET /states', () => {
    return request(app.getHttpServer())
      .get('/estados')
      .expect(200)
      .then(({ text }) => {
        const data = JSON.parse(text);
        expect(data).toHaveProperty('estados');
        expect(data['estados']).toBeArrayOfSize(26 + 1);
        expect(data['estados'][0]).toHaveProperty('nome');
        expect(data['estados'][0]).toHaveProperty('uf');
        expect(data['estados'][0]).toHaveProperty('populacao');
      });
  });
});
