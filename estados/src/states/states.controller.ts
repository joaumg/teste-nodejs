import { Controller, Get, Inject } from '@nestjs/common';
import { ClientGrpc } from '@nestjs/microservices';
import { Observable } from 'rxjs';

declare type State = {
  nome: string;
  uf: string;
  populacao: number;
};

declare type StatePopulation = {
  uf: string;
  populacao: number;
};

declare type StatesIndex = {
  estados: State[];
};

interface PopulationService {
  fetchAll({}): Observable<{ data: StatePopulation[] }>;
}

@Controller('estados')
export class StatesController {
  // https://pt.wikipedia.org/wiki/Unidades_federativas_do_Brasil
  private readonly mappedStates = {
    AC: { nome: 'Acre' },
    AL: { nome: 'Alagoas' },
    AP: { nome: 'Amapá' },
    AM: { nome: 'Amazonas' },
    BA: { nome: 'Bahia' },
    CE: { nome: 'Ceará' },
    DF: { nome: 'Distrito Federal' },
    ES: { nome: 'Espírito Santo' },
    GO: { nome: 'Goiás' },
    MA: { nome: 'Maranhão' },
    MT: { nome: 'Mato Grosso' },
    MS: { nome: 'Mato Grosso do Sul' },
    MG: { nome: 'Minas Gerais' },
    PA: { nome: 'Pará' },
    PB: { nome: 'Paraíba' },
    PR: { nome: 'Paraná' },
    PE: { nome: 'Pernambuco' },
    PI: { nome: 'Piauí' },
    RJ: { nome: 'Rio de Janeiro' },
    RN: { nome: 'Rio Grande do Norte' },
    RS: { nome: 'Rio Grande do Sul' },
    RO: { nome: 'Rondônia' },
    RR: { nome: 'Roraima' },
    SC: { nome: 'Santa Catarina' },
    SP: { nome: 'São Paulo' },
    SE: { nome: 'Sergipe' },
    TO: { nome: 'Tocantins' },
  };
  private populationService: PopulationService;
  constructor(@Inject('PopulationService') private client: ClientGrpc) {}

  onModuleInit() {
    this.populationService = this.client.getService<PopulationService>(
      'PopulationService',
    );
  }

  @Get('/')
  async states(): Promise<StatesIndex> {
    const { data: populationStates } = await this.populationService
      .fetchAll({})
      .toPromise();
    const mappedStatesPopulation = this.mappedStates;
    populationStates.forEach((state) => {
      mappedStatesPopulation[state.uf].populacao = state.populacao;
    });
    const states: State[] = Object.keys(mappedStatesPopulation).map((key) => {
      return {
        nome: mappedStatesPopulation[key].nome,
        uf: key,
        populacao: mappedStatesPopulation[key].populacao['low'] as number,
      };
    });
    return {
      estados: states,
    };
  }
}
