import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { StatesController } from './states/states.controller';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'PopulationService',
        transport: Transport.GRPC,
        options: {
          url: '127.0.0.1:5000',
          package: 'population',
          protoPath: join(__dirname, './states/population.proto'),
        },
      },
    ]),
  ],
  controllers: [AppController, StatesController],
  providers: [AppService],
})
export class AppModule {}
